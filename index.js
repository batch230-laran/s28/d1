// CRUD Operations (Create, Read, Update, Delete)


/*

- CRUD operations are the heart of any backend application

- Mastering the CRUD operations is essential for any developer 

- This helps in building character and increasing exposure to logical statements that will help us manipulate our data

- Mastering the CRUD operations of any language makes us a vauable developer and makes the work easier for us to deal with huge amount of information

*/



// INSERTING DOCUMENTS (CREATE)


/*

Syntax:
	- db.collectionName.insertOne({object});


*/



// INSERT ONE


db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "87654321",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
})





// -----------------------------------------

// INSERT MANY (inserts one or more documents)



/*

Syntax:
	- db.collectionName.insertMany([{objectA}, {objectB}]);


*/



db.users.insertMany(
	[
		{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact: {
				phone: "87000",
				email: "stephenhawking@gmail.com"
			},
			courses: ["Python", "React", "PHP"],
			department: "none"
		}, 

		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact: {
				phone: "4770220",
				email: "neilarmstrong@gmail.com"
			},
			courses: ["React", "Laravel", "MongoDB"],
			department: "none"
		}
	]
)






// -----------------------------------------

// FINDING DOCUMENTS (READ)



/*

Syntax:
	- db.collectionName.find();
	- db.collectionName.find({field: value});


*/


// retrieve all documents
db.users.find();



// Retrieves a specific document/s
db.users.find({firstName: "Stephen"});


// multiple criteria
db.users.find({lastName: "Armstrong", age: 82});








// -----------------------------------------

// DELETE DOCUMENTS (DELETE)



/*

Syntax:
	- db.collectionName.deleteOne({field: value});
	- db.collectionName.deleteMany({field: value});


*/



// deletes one document that matches the field and value
db.users.deleteOne({
	firstName: "Jane",

})


// deletes all document that matches the field and value
db.users.deleteMany({
	department: "HR",

})



// -----------------------------------------

// UPDATE



/*

Syntax:
	- db.collectionName.updateOne({criteria}. {$set: {field: value}});


*/


db.users.insertOne({
    firstName: "Test",
    lastName: "Test",
    age: 0,
    contact: {
        phone: "00000000",
        email: "test@gmail.com"
    },
    courses: [],
    department: "none"
});



// Update one document


db.users.updateOne(
	{ firstName: "Test" },
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact: {
				phone: "12345678",
				email: "imrich@gmail.com"
			},
			courses: ["PHP", "Laravel", "HTML"],
			department: "Operations",
			status: "active"
		}
	}
)


// another example


db.users.updateOne(
	{firstName: "Jane"},
	{
		$set:
		{
			firstName:"Jenny"
		}
	}
)



db.users.find();




// Updates multiple documents that matches field and value 

db.users.updateMany(
	{department: "none"},
	{
		$set:
		{
			department:"HR"
		}
	}
)





// -----------------------------------------

// REPLACE



/*

Syntax:
	- db.collectionName.replaceOne({criteria}. {$set: {field: value}});


*/

// replaces all the fields in a document not just a part 


db.users.replaceOne(
	{firstName: "Bill"}, 
	{
		firstName: "Elon",
		lastName: "Musk",
		age: 30,
		courses: ["PHP", "Laravel", "HTML"],
		status: "trippings"
	}
)

